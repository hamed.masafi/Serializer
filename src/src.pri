INCLUDEPATH += $$PWD

SOURCES += \
    $$PWD/abstractserializer.cpp \
    $$PWD/stringserializer.cpp \
    $$PWD/binaryserializer.cpp \
    $$PWD/sqlserializer.cpp

HEADERS += \
    $$PWD/serializer_global.h \
    $$PWD/abstractserializer.h \
    $$PWD/stringserializer.h \
    $$PWD/binaryserializer.h \
    $$PWD/sqlserializer.h

